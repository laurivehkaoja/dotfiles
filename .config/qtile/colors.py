# If using transparency, make sure you add (background="#00000000") to 'Screen' line(s).
dracula  = [
    ["#282a36", "#282a36"], # bg
    ["#f8f8f2", "#f8f8f2"], # fg
    ["#000000", "#000000"], # color01
    ["#ff5555", "#ff5555"], # color02
    ["#50fa7b", "#50fa7b"], # color03
    ["#f1fa8c", "#f1fa8c"], # color04
    ["#bd93f9", "#bd93f9"], # color05
    ["#ff79c6", "#ff79c6"], # color06
    ["#ffb86c", "#ffb86c"], # color07
    ["#9aedfe", "#9aedfe"]  # color15
    ]

