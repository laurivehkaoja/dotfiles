#!/usr/bin/env bash 

# Draw the wallpaper
nitrogen --restore &

# Start the notification deamon
dunst &
