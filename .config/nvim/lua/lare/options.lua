local options = {
    backup = false,
    fileencoding = "utf-8",
    ignorecase = true,
    smartcase = true,
    smartindent = true,
    swapfile = false,
    tabstop = 4,
    shiftwidth = 4,
	number = true
}
	
vim.opt.shortmess:append "c"

for k, v in pairs(options) do
  vim.opt[k] = v
end
