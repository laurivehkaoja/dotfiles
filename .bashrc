#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# PATH
if [ -d "$HOME/Applications" ] ;
  then PATH="$HOME/Applications:$PATH"
fi

## Editors
export VISUAL=vim
export EDITOR=vim

# Aliases

## Essentials
alias ls='eza -alh'
alias mkdir='mkdir -p'
alias grep='rg'

## Programs
alias vi='nvim'
alias vim='nvim'

## Moving around
alias dl='cd ~/Downloads'
alias docs='cd ~/Documents'
alias vids='cd ~/Videos'
alias pics='cd ~/Pictures'
alias apps='cd ~/Applications'
alias repos='cd ~/Documents/Repositories'

## Wifi control
alias wifi='nmcli dev wifi'
alias connect='nmcli dev wifi connect' # connect <ssid> password <pwd>

## Pacman
alias unlock='sudo rm /var/lib/pacman/db.lck'    # remove pacman lock
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)' # remove orphaned packages

# System management
alias shutdown='shutdown now'
alias sudo='doas --'

# Custom functions

## UP function
up () {
  local d=""
  local limit="$1"

  # Default to limit of 1
  if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
    limit=1
  fi

  for ((i=1;i<=limit;i++)); do
    d="../$d"
  done

  # perform cd. Show error if cd fails
  if ! cd "$d"; then
    echo "Couldn't go up $limit dirs.";
  fi
}

## Extraction function ex
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


PS1='[\u@\h \W]\$ '

# Cargo stuff 
. "$HOME/.cargo/env"

